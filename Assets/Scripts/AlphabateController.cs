using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphabateController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name != "Alphabet")
        {
            gameObject.transform.Translate((Vector3.down * 0.05f));
            if(gameObject.transform.position.y < -10f)
                Destroy(gameObject);
        }
    }
}
