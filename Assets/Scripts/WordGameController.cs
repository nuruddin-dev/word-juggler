using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class WordGameController : MonoBehaviour
{
    // public Animator gifAnimator;
    public GameObject alphabetPrefab;
    public Transform spawnPoint = new RectTransform();
    public Text scoreText;
    public Text missText;
    [FormerlySerializedAs("word")] public TextMeshProUGUI wordTMP;
    public TextMeshProUGUI error;
    public GameObject fill;    // Reference to the Fill GameObject
    public GameObject handle;  // Reference to the Handle GameObject
    public float totalTime = 60f;  // Default total time in seconds

    private List<GameObject> _spawnedAlphabets = new List<GameObject>();
    private int _score = 0;
    private int _misses = 0;
    private bool _isWordsLoaded;

    // private string[] words = { "UNITY", "GAME", "DEVELOPMENT", "PLAYER", /* Add more words as needed */ };
    private List<char> _currentWord = new List<char>();
    
    private List<string> _validEnglishWords = new List<string>();

    void Start()
    {
        // Load the list of valid English words from the file
        LoadValidEnglishWords();
    }

    void Update()
    {
        if (_isWordsLoaded)
        {
            CheckInput();
            // CheckOutOfBounds();
            CheckGameEnd();
            float remainingTime = Mathf.Clamp(totalTime - Time.time, 0f, totalTime);  // Calculate the remaining time
            float interpolationTime = 1f - remainingTime / totalTime;
            if (remainingTime >= 0)
            {
                Timer(interpolationTime);
            }
        }
        
    }

    IEnumerator SpawnAlphabets()
    {
            while (true)
            {
                // Array of specific float values
                float[] floatValues = {-2.0f, -1.5f, -1.0f, -0.5f, 0.0f, 0.5f, 1.0f, 1.5f, 2.0f};

                // Generate a random index to select from the array
                int randomIndex = Random.Range(0, floatValues.Length -1);
                // Get the random float from the array
                float randomFloat = floatValues[randomIndex];
        
                GameObject alphabet = Instantiate(alphabetPrefab, new Vector3(randomFloat, 5f, 0f), Quaternion.identity);
                char randomChar = GetRandomChar();

                // Find the TextMeshProUGUI component, whether it's a direct child or nested
                TextMeshPro textComponent = alphabet.GetComponentInChildren<TextMeshPro>();
                if (textComponent == null)
                {
                    textComponent = alphabet.GetComponent<TextMeshPro>();
                }

                if (textComponent != null)
                {
                    textComponent.text = randomChar.ToString();
                }
                else
                {
                    Debug.LogError("TextMeshPro component not found in alphabetPrefab or its children.");
                }

                // _currentWord.Add(randomChar);
                // _spawnedAlphabets.Add(alphabet);

        
                // misses++;
                // UpdateMisses();
                yield return new WaitForSeconds(Random.Range(0.5f, 1f)); // Adjust spawn interval as needed

            }

    }




    char GetRandomChar()
    {
        char randomChar = (char)('A' + Random.Range(0, 26));
        return randomChar;
    }

    void CheckInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                GameObject hitObject = hit.collider.gameObject;
                TextMeshPro hitText = hitObject.GetComponentInChildren<TextMeshPro>();
                
                if (hitText != null)
                {
                    char selectedChar = hitText.text[0];
                    Destroy(hitObject);
                    Debug.Log($"Destroyed: {selectedChar}");
                    // if (currentWord.Contains(selectedChar))
                    // {
                    //     currentWord.Remove(selectedChar);
                    //     score++;
                    //     UpdateScore();
                    //     Destroy(hitObject);
                    // }
                    // else
                    // {
                    //     misses++;
                    //     UpdateMisses();
                    // }
                }
            }
        }
    }

    void UpdateScore()
    {
        // scoreText.text = "Score: " + score;
    }

    void UpdateMisses()
    {
        missText.text = "Misses: " + _misses;

        if (_misses >= 10)
        {
            GameOver();
        }
    }

    void CheckOutOfBounds()
    {
        foreach (GameObject alphabet in _spawnedAlphabets)
        {
            if (alphabet.transform.position.y < -5f)
            {
                Destroy(alphabet);
                _misses++;
                UpdateMisses();
            }
        }

        _spawnedAlphabets.RemoveAll(alphabet => alphabet == null);
    }

    void CheckGameEnd()
    {
        if (_score >= 50) // Adjust the winning condition as needed
        {
            GameOver(true);
        }
    }

    void GameOver(bool victory = false)
    {
        if (victory)
        {
            Debug.Log("You win!");
        }
        else
        {
            Debug.Log("Game over!");
        }

        // You can add more game over logic, such as displaying a UI panel, resetting the game, etc.
        // For now, let's stop the game.
        StopAllCoroutines();
    }

    public void ResetWord()
    {
        wordTMP.text = wordTMP.text.Substring(0, wordTMP.text.Length - 1);
    }

    // public void CheckWord()
    // {
    //     String text = word.text;
    //     if (words.Contains(text))
    //     {
    //         score += text.Length;
    //     }
    //     word.text = "";
    // }
    
    public void CheckWord()
    {
        string enteredWord = wordTMP.text.ToLower(); // Convert to lowercase for case-insensitive comparison

        if (_validEnglishWords.Contains(enteredWord))
        {
            _score += enteredWord.Length;
            Debug.Log($"score: {_score}");
            // Play the GIF animation
            // gifAnimator.SetTrigger("PlayGIF");
            UpdateScore();
            wordTMP.text = "";
            error.text = _score.ToString();
        }
    }

    void LoadValidEnglishWords()
    {
        TextAsset englishWords = Resources.Load<TextAsset>("english_words");
        if (englishWords != null)
        {
            string[] wordArray = englishWords.text.Split('\n');
            _validEnglishWords = new List<string>(wordArray.Select(word => word.Trim().ToLower()));
            StartCoroutine(SpawnAlphabets());
            _isWordsLoaded = true;
            // Now, 'englishWordsList' contains your list of words
        }
        else
        {
            error.text += "Failed to load 'english_words' from Resources.\n";
            Debug.LogError("Failed to load 'english_words.txt' from Resources.");
        }

    }

    void Timer(float interpolationTime)
    {
        // Calculate the positions based on the current time
        float handlePosition = Mathf.Lerp(-40f, 40f, interpolationTime);
        float fillPosition = Mathf.Lerp(-40f, 0f, interpolationTime);

        // Set the positions to the GameObjects
        handle.transform.localPosition = new Vector3(handlePosition, 0f, 0f);
        fill.transform.localPosition = new Vector3(fillPosition, 0f, 0f);

        // Set the width of the Fill GameObject
        RectTransform fillRectTransform = fill.GetComponent<RectTransform>();
        fillRectTransform.sizeDelta = new Vector2(80f * interpolationTime, fillRectTransform.sizeDelta.y);
    }
}
